
import numpy as np
import pandas as pd 
import faiss
from sentence_transformers import SentenceTransformer
import os
from db1 import get_question_data

class Question:
    
    def __init__(self):
        print("----------loading model/embeddings----------")
        if not os.path.exists(f"utils/model"):
            print("making directory to save model")
            # make directory
            os.makedirs(f"utils/model") 
            # save model
            self.model = SentenceTransformer('bert-base-nli-mean-tokens')
            self.model.save("utils/model")
        else:
            # load model
            print("model is already save")
            self.model = SentenceTransformer("utils/model")
            
    def __loadData(self):
        """
            load data and load embeddings
        """
        # load questions
        
        # questions = get_question_data()
        
        questions = pd.read_csv("utils/questions.csv")
        questions = questions['questions']

        # load embeddings
        sentence_embeddings = np.load("utils/embedding.npy")

        # print(sentence_embeddings.shape)
        return sentence_embeddings, questions

    def prediction(self, qus, max_num):
        # encode the question
        xq = self.model.encode([qus])        
        
        # load sentence_embeddings and questions
        sentence_embeddings, questions =  self.__loadData()
        
        # making indexes 
        d = sentence_embeddings.shape[1]
        index = faiss.IndexFlatL2(d)
        index.add(sentence_embeddings)

        # generating predictions
        # save 
        res = []
        D, I = index.search(xq, max_num)
        result = I[0].tolist()
        for num in result:
            res.append(questions[num])

        return res[1:]




