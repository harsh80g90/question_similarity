CREATE TABLE question_post (
	id SERIAL PRIMARY KEY,
	u_id INTEGER,
	question TEXT,
	created_at TIMESTAMP,
	updated_at TIMESTAMP,
    active BOOLEAN 
);