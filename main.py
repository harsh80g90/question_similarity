from fastapi import FastAPI
import uvicorn

from trainModel import Question

# make object for the question
api = Question()

# init fastAPI
app = FastAPI()

@app.get('/question/')
@app.post('/question/')
async def predict(question:str, max_num:int):    
    max_num = int(max_num)+1

    try:
        value = api.prediction(question, max_num)
    except Exception as e:
        print("error occured", e)
        value = {}

    return {"questions":value}

if __name__ == '__main__':
    uvicorn.run(app, port=8080, host="143.198.113.228")

# uvicorn main:app --host 143.198.113.228 --port 8000