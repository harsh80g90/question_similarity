import faiss
import pandas as pd


from sentence_transformers import SentenceTransformer
model = SentenceTransformer('bert-base-nli-mean-tokens')


class Questions:
    def __init__(self):
        print("model stated")

    
    def getData(self, path):
        """
            input -> data path
            output -> preprocessed data 
        """
        data = pd.read_csv(path)
        
        data.drop_duplicates(inplace=True)
        data.dropna(inplace=True)
        data = data.reset_index()
        
        return data['questions']

    def train(self):
        """
            input -> None
            output -> index , question
        """
        question = self.getData()

        # making sentence embeddings
        sentence_embeddings = model.encode(question)

        #sentence_embeddings.shape

        #faiss work
        d = sentence_embeddings.shape[1]
        index = faiss.IndexFlatL2(d)

        index.add(sentence_embeddings)

        return index, question

    def prediction(self, qus, max_num):
        """
            input -> qus , max_num
            output -> retrun similar questions for the give question
        """
        xq = model.encode([qus])
        # get the index and question
        index, question = self.train()

        D, I = index.search(xq, max_num)
        result = I[0].tolist()
        for num in result:
            print(question[num])
			
			

