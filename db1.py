
import os
import psycopg2
import pandas as pd
#import mysql.connector

def get_PSQLconnection():
    
    connection = psycopg2.connect(
                                host=os.getenv("PDBHOST"),
                                user=os.getenv("PDBUSER"),
                                password=os.getenv("PDBPASS"),
                                dbname=os.getenv("PDBNAME"),
                                port=os.getenv("PDBPORT")
                            )

    return connection


# def get_connection():
#     db = mysql.connector.connect(
#             host=os.getenv("PDBHOST"),
#             user=os.getenv("PDBUSER"),
#             password=os.getenv("PDBPASS"),
#             database=os.getenv("PDBNAME")
#         )
#     return db

def get_question_data():
    
    db = get_PSQLconnection()
    cur = db.cursor()
    data = []
    
    query = "select question from question_post"
    cur.execute(query)
    job_list = cur.fetchall()

    for row in job_list:
        data.append(row)

    cur.close()
    db.close()

    return data

def store_data():
    
    db = get_PSQLconnection()
    cur = db.cursor()
    
    main_data = pd.read_csv("questions.csv")
    
    questions = main_data['questions']
    
    for ques in questions:

        ques = ques.replace("\"", " ").replace("'", " ").replace("\\","")
        query = f"insert into question_post (question) values ('{ques}')" 
        cur.execute(query, ques)
        print(len(ques))
        
        db.commit()
    